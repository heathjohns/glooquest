use cc;
  
fn main() {
    // Compile aux.c
    cc::Build::new()
        .file("c/aux.c")
        .include("/usr/local/include")
        .compile("aux");

    println!("cargo:rustc-link-search=native=/usr/local/lib");
    println!("cargo:rustc-link-lib=GL");
    println!("cargo:rustc-link-lib=glfw");
    println!("cargo:rustc-link-lib=epoxy");
}
