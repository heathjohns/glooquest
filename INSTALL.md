# Prerequisites

You'll need the "source" packages installed for:

GLFW3   https://www.glfw.org  
Expoxy  https://github.com/anholt/libepoxy

Before running 'cargo run'


# Optional installs

Aseprite  https://www.aseprite.org  
Tiled     https://www.mapeditor.org

And Python3 for conversion scripts
