#![allow(unused_imports, dead_code)]

/*

=Architecture

The "screen" is a fixed 320x180@60Hz, with 1 byte RGB - 3 bits for R, 3 bits
for G, and 2 bits for B.  Every frame the entire buffer gets copied up to
a texture on the GPU, which decodes the RGB and upscales the pixels.  The
transfer to the GPU is the slow part, but it's only ~56kB so it's still pretty
quick.  The aux.c file is where this all happens, leaving the rust side with
creating the contents of the pixel buffer.

On my underpowered desktop you can copy the entire 56kB buffer from one place
to another ~250 times per *millisecond*, so the traditional sprite-drawing
getup isn't necessary - an Amiga style "bob" way of doing things will
be plenty fast to do anything 2D.  Though it might be worth putting the blit
operation in aux.c to make sure the (highly optimized) memset() and memcpy()
functions are being used well.

Input is just a gamepad (emulated on the keyboard, or a real one) - one dpad
and A/B buttons.  This comes in every frame as a bit field.  No sound yet!

*/

use std::{
    path::{Path, PathBuf},
    io::{self, Read},
    fs,
};

extern "C" {
    fn gl_init();
    fn gl_display(pixels: *const u8) -> u32;
    fn gl_clear(pixels: *const u8, color: u8);
}

const IN_UP: u32 = 0b0000_0001;
const IN_DOWN: u32 = 0b0000_0010;
const IN_LEFT: u32 = 0b0000_0100;
const IN_RIGHT: u32 = 0b0000_1000;
const IN_A: u32 = 0b0001_0000;
const IN_B: u32 = 0b0010_0000;
const IN_QUIT: u32 = 1 << 31;
const TRANSP: u8 = 226;
const PIX_BUF_REQUIRED_SIZE: usize = 65536;


struct Tile {
    width: u32,
    height: u32,
    pixels: Vec<u8>,
}

impl Tile {
    fn from_path(path: &Path) -> Self {
        println!("Opening tile: {:?}", path);
        let mut bytes = Vec::new();
        fs::File::open(path).unwrap().read_to_end(&mut bytes).expect("failed to open file");
        let width = bytes[0] as u32;
        Tile {
            width,
            height: width / (bytes.len() as u32 - 1),
            pixels: bytes.split_off(1),
        }
    }
    fn blit(&self, dest: &mut [u8], stride: Option<u32>, x: u32, y: u32) {
        let stride = stride.unwrap_or(320) as usize;
        let mut loc = y as usize * stride + x as usize;
        let mut tile_x = 0;
        for color in self.pixels.iter().copied() {
            if color != TRANSP {
                // TODO: Running off the ends of dest is predictable - can precalc and speed this up
                if let Some(p) = dest.get_mut(loc) {
                    *p = color;
                }
            }
            tile_x += 1;
            loc += 1;
            if tile_x == self.width {
                loc += stride - self.width as usize;
                tile_x = 0;
            }
        }
    }
}

fn main() {
    // Init the display
    unsafe { gl_init() };

    // Set up state
    let mut pixels: Vec<u8> = Vec::with_capacity(PIX_BUF_REQUIRED_SIZE);
    pixels.resize(PIX_BUF_REQUIRED_SIZE, 0);
    let mut hx = 184;
    let mut hy = 90;
    let mut dx = 120;
    let mut dy = 90;
    let heath_tiles = vec![
        Tile::from_path(&PathBuf::from("assets/sprites/heath_0.raw")),
        Tile::from_path(&PathBuf::from("assets/sprites/heath_1.raw")),
    ];
    let darryl_tiles = vec![
        Tile::from_path(&PathBuf::from("assets/sprites/darryl_0.raw")),
        Tile::from_path(&PathBuf::from("assets/sprites/darryl_1.raw")),
    ];

    // Main loop
    let mut last_input = 0;
    let mut character = 1;
    loop {
        let input = unsafe { gl_display(pixels.as_ptr()) };

        let x = if character == 0 { &mut hx } else { &mut dx };
        let y = if character == 0 { &mut hy } else { &mut dy };

        if input & IN_QUIT != 0 {
            break;
        }
        if input & IN_UP != 0 && *y > 0 {
            *y -= 1;
        }
        if input & IN_DOWN != 0 && *y < 147 {
            *y += 1;
        }
        if input & IN_LEFT != 0 && *x > 0 {
            *x -= 1;
        }
        if input & IN_RIGHT != 0 && *x < 303 {
            *x += 1;
        }
        if input & IN_A != 0 && last_input & IN_A == 0 {
            character ^= 1;
        }
        
        unsafe { gl_clear(pixels.as_ptr(), 9) };

        // Draw
        let h_tile = &heath_tiles[if hx & 0b100 == 0 { 0 } else { 1 }];
        h_tile.blit(&mut pixels, None, hx, hy);
        let d_tile = &darryl_tiles[if dx & 0b100 == 0 { 0 } else { 1 }];
        d_tile.blit(&mut pixels, None, dx, dy);

        last_input = input;
    }
}
