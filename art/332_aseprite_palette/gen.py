header = '''GIMP Palette
# 
# by Heath Johns
# http://heathjohns.com/
#'''

def main():
    print(header)
    for i in range(256):
        r = int((i >> 5) / 7 * 255)
        g = int(((i >> 2) & 0b111) / 7 * 255)
        b = int((i & 0b11) / 3 * 255)
        h = r << 16 | g << 8 | b
        print('{: >3} {: >3} {: >3} {:0>6x}'.format(r, g, b, h))

main()
