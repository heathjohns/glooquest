#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <epoxy/gl.h>
#include <epoxy/glx.h>
#include <GLFW/glfw3.h>
#include "linmath.h"

#define IN_UP    0b00000001
#define IN_DOWN  0b00000010
#define IN_LEFT  0b00000100
#define IN_RIGHT 0b00001000
#define IN_A     0b00010000
#define IN_B     0b00100000
#define IN_QUIT  1 << 31

#define UNUSED(x) (void)(x)

uint32_t GlInput = 0;
GLFWwindow *GlWin = NULL;
GLuint GlProgram = 0;
// The quad
struct {
    float x, y;
    float u, v;
} Vertices[6] = {
    { -1.7777777f,  1.f,   0.f,   0.f },
    {  1.7777777f, -1.f, 320.f, 180.f },
    { -1.7777777f, -1.f,   0.f, 180.f },

    { -1.7777777f,  1.f,   0.f,   0.f },
    {  1.7777777f,  1.f, 320.f,   0.f },
    {  1.7777777f, -1.f, 320.f, 180.f },
};

void gl_error_cb(int err, const char* text) {
    printf("Error %d: %s\n", err, text);
}

void gl_key_cb(GLFWwindow* win, int key, int scancode, int action, int mods) {
    UNUSED(scancode);
    UNUSED(mods);
    uint32_t button = 0;
    switch (key) {
        case GLFW_KEY_ESCAPE: {
            glfwSetWindowShouldClose(win, GLFW_TRUE);           
        }; break;
        case GLFW_KEY_UP:
        case GLFW_KEY_K:
        case GLFW_KEY_W: button = IN_UP; break;
        case GLFW_KEY_DOWN:
        case GLFW_KEY_J:
        case GLFW_KEY_S: button = IN_DOWN; break;
        case GLFW_KEY_LEFT:
        case GLFW_KEY_H:
        case GLFW_KEY_A: button = IN_LEFT; break;
        case GLFW_KEY_RIGHT:
        case GLFW_KEY_L:
        case GLFW_KEY_D: button = IN_RIGHT; break;
        case GLFW_KEY_Z:
        case GLFW_KEY_COMMA: button = IN_A; break;
        case GLFW_KEY_X:
        case GLFW_KEY_PERIOD: button = IN_B; break;
    };
    if (action == GLFW_PRESS) {
        GlInput |= button;
    } else if (action == GLFW_RELEASE) {
        GlInput &= ~button;
    }
}

void gl_poll_gamepad() {
    // Untested - FreeBSD X11 is lacking for joysticks -hj
    static uint32_t last_gamepad_input = 0;
    GLFWgamepadstate state;
    if (glfwGetGamepadState(GLFW_JOYSTICK_1, &state)) {
        uint32_t gamepad_input = 0;
        if (state.buttons[GLFW_GAMEPAD_BUTTON_DPAD_UP])
            gamepad_input |= IN_UP;
        if (state.buttons[GLFW_GAMEPAD_BUTTON_DPAD_DOWN])
            gamepad_input |= IN_DOWN;
        if (state.buttons[GLFW_GAMEPAD_BUTTON_DPAD_LEFT])
            gamepad_input |= IN_LEFT;
        if (state.buttons[GLFW_GAMEPAD_BUTTON_DPAD_RIGHT])
            gamepad_input |= IN_RIGHT;
        if (state.buttons[GLFW_GAMEPAD_BUTTON_A])
            gamepad_input |= IN_A;
        if (state.buttons[GLFW_GAMEPAD_BUTTON_B])
            gamepad_input |= IN_B;

        if (gamepad_input != last_gamepad_input)
            GlInput = gamepad_input;
    }
}

bool shader_log(char *name, GLuint shader) {
    GLint result, log_len;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_len);
    if (log_len > 0) {
        char *text = calloc(log_len + 1, 1);
        glGetShaderInfoLog(shader, log_len, NULL, text);
        printf("\nErrors in %s:\n", name);
        printf("%s", text);
        printf("\n");
        free(text);
    }
    return result != GL_FALSE;
}

void gl_init() {
    // Standard stuff
    const char* vertex_shader_text = "\
    #version 130\n\
    uniform mat4 MVP;\n\
    attribute vec2 vCoords;\n\
    attribute vec2 vPos;\n\
    varying vec2 coords;\n\
    void main() {\n\
        gl_Position = MVP * vec4(vPos, 0.0, 1.0);\n\
        coords = vCoords;\n\
    }\n";

    // Convert from 320x180 to 256x256x8, and to 3:3:2 colour space
    const char* fragment_shader_text = "\
    #version 130\n\
    varying vec2 coords;\n\
    uniform sampler2D tex_unit;\n\
    void main() {\n\
        int off = int(floor(coords.y) * 320 + floor(coords.x));\n\
        int y = off >> 8;\n\
        int x = off & 0xff;\n\
        int v = int(round(texture(tex_unit, vec2(x / 256.0, y / 256.0)).r * 255));\n\
        float r = float(v >> 5) / 7;\n\
        float g = float((v >> 2) & 7) / 7;\n\
        float b = float(v & 3) / 3;\n\
        gl_FragColor = vec4(r, g, b, 1.0);\n\
    }\n";

    // GLFW Init
    glfwSetErrorCallback(gl_error_cb);
    assert(glfwInit());
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    GlWin = glfwCreateWindow(1280, 720, "Glooquest", NULL, NULL);
    if (!GlWin) {
        glfwTerminate();
        assert(false);
    }
    glfwMakeContextCurrent(GlWin);
    glfwSetKeyCallback(GlWin, gl_key_cb);
    glfwSwapInterval(1); // 60Hz

    // Set up geometry
    GLuint vertex_buf;
    glGenBuffers(1, &vertex_buf);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);

    // Set up texture
    GLuint tex_name;
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &tex_name);
    glBindTexture(GL_TEXTURE_2D, tex_name);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R8, 256, 256);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    // Compile shaders and link program
    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    assert(vertex_shader != GL_FALSE);
    glShaderSource(vertex_shader, 1, &vertex_shader_text, NULL);
    glCompileShader(vertex_shader);
    assert(vertex_shader != GL_FALSE);
    shader_log("Vertex", vertex_shader);
    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_shader_text, NULL);
    glCompileShader(fragment_shader);
    assert(fragment_shader != GL_FALSE);
    assert(shader_log("Fragment", fragment_shader));
    GlProgram = glCreateProgram();
    glAttachShader(GlProgram, vertex_shader);
    glAttachShader(GlProgram, fragment_shader);
    glLinkProgram(GlProgram);

}

#define SCREEN_RATIO (1280 / 720.f)

// Note: pixels must be a >64kB buffer
uint32_t gl_display(uint8_t *pixels) {
    // Get shader locations and set up GLSL pointers
    glUseProgram(GlProgram);
    GLint mvp_location = glGetUniformLocation(GlProgram, "MVP");
    GLint vpos_location = glGetAttribLocation(GlProgram, "vPos");
    GLint vcoords_location = glGetAttribLocation(GlProgram, "vCoords");
    glEnableVertexAttribArray(vpos_location);
    glVertexAttribPointer(vpos_location, 2, GL_FLOAT, GL_FALSE,
                          sizeof(Vertices[0]), (void*) 0);
    glEnableVertexAttribArray(vcoords_location);
    glVertexAttribPointer(vcoords_location, 2, GL_FLOAT, GL_FALSE,
                          sizeof(Vertices[0]), (void*) (sizeof(float) * 2));
    GLint tex_unit_loc = glGetUniformLocation(GlProgram, "tex_unit");

    // Update display
    glViewport(0, 0, 1280, 720);
    glClear(GL_COLOR_BUFFER_BIT);
    // - Set up matrix
    mat4x4 m, p, mvp;
    mat4x4_identity(m);
    mat4x4_ortho(p, -SCREEN_RATIO, SCREEN_RATIO, -1.f, 1.f, 1.f, -1.f);
    mat4x4_mul(mvp, p, m);
    // - Update shaders & textures
    glUniformMatrix4fv(mvp_location, 1, GL_FALSE, (const GLfloat*) mvp);
    glProgramUniform1i(GlProgram, tex_unit_loc, 0);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 256, 256, GL_RED, GL_UNSIGNED_BYTE, pixels);
    // - Draw and flip
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glfwSwapBuffers(GlWin);
    glfwPollEvents();
    gl_poll_gamepad();

    if (glfwWindowShouldClose(GlWin)) {
        glfwDestroyWindow(GlWin);
        glfwTerminate();
        return IN_QUIT;
    } else {
        return GlInput;
    }
}

// Couldn't find (with a quick search) for a fast way to do this in rust
void gl_clear(uint8_t *pixels, uint8_t color) {
    memset(pixels, color, 320 * 180);
}

#ifdef DEBUG_MAIN

#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

int main() {
    uint8_t pixels[65536];

    // Debug load img
    int fd = open("tex64k.raw", O_RDONLY);
    assert(fd >= 0);
    uint8_t *pix = mmap(0, 65536, PROT_READ, MAP_SHARED, fd, 0);
    assert(pix >= 0);
    for (int i = 0; i < 65536; i++)
        pixels[i] = pix[i];
    munmap(pix, 65536);
    close(fd);

    // Run
    gl_init();
    for (;;) {
        uint32_t input = gl_display(pixels);
        if (input & IN_QUIT)
            break;
        // Update state
        if (input & IN_UP)
            memmove(pixels+320, pixels, 320*180);
        if (input & IN_DOWN)
            memmove(pixels, pixels+320, 320*180);
        if (input & IN_LEFT)
            memmove(pixels, pixels+1, 320*180);
        if (input & IN_RIGHT)
            memmove(pixels+1, pixels, 320*180);
    }
}

#endif // DEBUG_MAIN
